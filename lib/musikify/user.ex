defmodule Musikify.User do
  use Ecto.Schema
  import Ecto.Changeset

  alias Musikify.SearchHistoryItem


  schema "users" do
    field :age, :integer
    field :name, :string
    has_many :search_history_items, SearchHistoryItem

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :age])
    |> validate_required([:name, :age])
  end
end
