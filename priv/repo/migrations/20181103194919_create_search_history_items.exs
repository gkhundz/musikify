defmodule Musikify.Repo.Migrations.CreateSearchHistoryItems do
  use Ecto.Migration

  def change do
    create table(:search_history_items) do
      add :query, :string
      add :created_at, :naive_datetime
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:search_history_items, [:user_id])
  end
end
